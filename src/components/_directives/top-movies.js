'use strict';
angular.module("movieBase")
	.directive('topMovies', function(){
		// Runs during compile
		return {
			// name: '',
			// priority: 1,
			// terminal: true,
			scope: {
				playlist: '@'
			}, 
			controller: function($scope, $element, $attrs, $transclude) {
				// $http.get("http://gdata.youtube.com/feeds/api/playlists/"+$scope.playlist+"?v=2&alt=json")
				// .success(function(response) {
				//   $scope.tops = response['feed']['entry'];
				// });
			},
			// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
			restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
			template: '<table class="table" style="width: auto"><tr ng-repeat="movie in tops | limitTo:7"><td><span  class="glyphicon glyphicon-play-circle"></span></td><td><a href="{{movie.link[0].href}}">{{movie.title.$t | limitTo:20}}...</a></td></tr></table>',
			// templateUrl: '',
			replace: true,
			// transclude: true,
			// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
			link: function($scope, iElm, iAttrs, controller) {
				
			}
		};
	});