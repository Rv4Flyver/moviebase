'use strict';
// converts horizontal text to vertical
angular.module('movieBase')
	.directive('verticalText', function($compile){
		// Runs during compile
		return {
			// name: '',
			// priority: 1,
			// terminal: true,
			scope: false,
			// controller: function($scope, $element, $attrs, $transclude) {},
			// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
			restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
			template: '',
			// templateUrl: '',
			replace: true,
			// transclude: true,
			// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
			link: function(scope, iElm, iAttrs, controller) {
				var title = iElm.html();
				var verticalTitle = "";
				for(var i=0; i< title.length; i++)
				{
					verticalTitle += title[i]+"<br>";
				}
				iElm.html(verticalTitle);
				$compile(iElm.contents())(scope);
			}
		};
	});