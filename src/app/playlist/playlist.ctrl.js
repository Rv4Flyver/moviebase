'use strict';

angular.module('movieBase')
  .controller('PlaylistController', function ($log,$scope,youtubeService,infoService,storageService) {
    var vm = this;

    var movies = [];
    // pagination
    vm.currentPage = 1;
    vm.totalItems = 0;
    vm.maxSize = 8;
    vm.bigTotalItems = 175;
    vm.bigCurrentPage = 1;
    
    vm.defaultPlaylist = storageService.youtube.getPlaylists()[0];  // take first playlist as default
    vm.playlists = "";
    vm.movies = [
      // {
      //   title : '',
      //   description: '',
      //   thumbnails: {
      //     p90 : '',
      //     p180: '',
      //     p360: '',
      //     p480: '',
      //     p720: ''
      //   },
      //   videoId: ''
      // }
    ];
    //var fetchedResult = {};

    youtubeService.getPlaylistItems(vm.defaultPlaylist, function (result) {
        $log.log('> youtubeService.getPlaylist:');
        $log.info(result);

        movies = result;
        vm.totalItems = result.length;
        vm.pageChanged();
    });

    vm.getPlaylists = function() {
        youtubeService.getPlaylistItems(vm.playlists, function (result) {
            $log.log('> youtubeService.getPlaylist:');
            $log.info(result);

            movies = result;
            vm.totalItems = result.length;
            vm.pageChanged();
        });

        // // Get playlists in loop
        // var playlistsArr = vm.playlists.split(',');
        // for(var p in playlistsArr) {
        //     youtubeService.getPlaylist(playlistsArr[p], function (result) {
        //         $log.log('> youtubeService.getPlaylist:');
        //         $log.info(result);

        //         movies = movies.concat(result);
        //         vm.totalItems =  movies.length; 
        //         vm.pageChanged();
        //     });
        // }
    }

    vm.pageChanged = function() {
        vm.movies = movies.slice(vm.maxSize*(vm.currentPage-1),vm.maxSize*(vm.currentPage-1)+vm.maxSize);
        infoService.setMoviesCount(movies.length);
    };


    angular.forEach($scope.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
    });
  });
