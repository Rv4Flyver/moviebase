'use strict';

var youtubeAPI = 'https://www.googleapis.com/youtube/v3/';

angular.module('movieBase', ['ngAnimate', 'ngResource', 'ui.router', 'ui.bootstrap'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('base', {
        url: '',
        templateUrl: 'app/base/base.view.html',
        controller: 'BaseController',
        controllerAs: 'BaseCtrl'
      })
      .state('base.home', {
        url: '/home',
        templateUrl: 'app/main/main.view.html',
        controller: 'MainController',
        controllerAs: 'MainCtrl'
      })
       .state('base.about', {
        url: '/about',
        templateUrl: 'app/about/about.view.html',
        controller: 'AboutController',
        controllerAs: 'AboutCtrl'
      })
      // CHANNELS
        .state('base.channel', {
        url: '/channel',
        templateUrl: 'app/channel/channel.view.html',
        controller: "ChannelController",
        controllerAs: 'ChannelCtrl'
      })
         .state('base.channel.watch', {
          url: '/:id',
          templateUrl: 'app/channel.watch/channel.watch.view.html',
          controller: "ChannelWatchController",
          controllerAs: 'ChannelWatchCtrl'
        })
      // PLAYLISTS
        .state('base.playlist', {
        url: '/playlist',
        templateUrl: 'app/playlist/playlist.view.html',
        controller: "PlaylistController",
        controllerAs: 'PlaylistCtrl'
      })
         .state('base.playlist.watch', {
          url: '/:id',
          templateUrl: 'app/playlist.watch/playlist.watch.view.html',
          controller: "PlaylistWatchController",
          controllerAs: 'PlaylistWatchCtrl'
        })
      // MOVIES
        .state('base.movie', {
        url: '/movie',
        templateUrl: 'app/movie/movie.view.html',
        controller: "MovieController",
        controllerAs: 'MovieCtrl'
      })     
         .state('base.movie.watch', {
          url: '/:id',
          templateUrl: 'app/movie.watch/movie.watch.view.html',
          controller: "MovieWatchController",
          controllerAs: 'MovieWatchCtrl'
        })
         .state('base.movie.edit', {
          url: '/:id/edit',
          templateUrl: 'app/movie.edit/movie.edit.view.html',
          controller: "MovieEditController",
          controllerAs: 'MovieEditCtrl'
        })       
       ;

    $urlRouterProvider.otherwise('/');
  })

  .constant('youtubeConsts', {
          apiKey: 'AIzaSyDi9Jq6Ew4gwALVaMcfej6Cy9d2lETujGU',
          apiEndpoints: {
            'playlistItems' : youtubeAPI + 'playlistItems?',
            'videos'        : youtubeAPI + 'videos?',
            'channels'      : youtubeAPI + 'channels?',
            'playlists'     : youtubeAPI + 'playlists?',
            'subscriptions' : youtubeAPI + 'subscriptions?'
          },
          embedURL: "https://www.youtube.com/embed/"
    });
