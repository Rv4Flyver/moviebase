'use strict';

angular.module('movieBase')
  .controller('MovieWatchController', function ($scope,$sce,$stateParams,youtubeConsts) {
    var vm = this;
    vm.id = $stateParams.id;
    vm.url = $sce.trustAsResourceUrl( youtubeConsts.embedURL+ $stateParams.id);

  });
