'use strict';

angular.module('movieBase')
       .service('infoService', infoService);

function infoService($rootScope, youtubeConsts) 
{
    var vm = this;
    vm.info = {
        stats: {
            movies: {
                count: 0
            },
            playlists: {
                count: 0
            }
        }
    };
    vm.setMoviesCount = function(count) {
        vm.info.stats.movies.count = count;
        notifyAboutUserDataChanges();
    };

    vm.setPlaylistsCount = function(count) {
        vm.info.stats.playlists.count = count;
        notifyAboutUserDataChanges();
    };

    // Events Subscription & Handling   
    vm.subscribeOnUserDataChanges = function(scope, callback) {
            var handler = $rootScope.$on('info-data-event', callback);
            scope.$on('$destroy', handler);
        };
    
    function notifyAboutUserDataChanges() {
        $rootScope.$emit('info-data-event');
    }
}
