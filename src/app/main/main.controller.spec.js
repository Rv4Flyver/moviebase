describe('mocking service http call', function() {
  beforeEach(module('movieBase'));

  var MainCtrl, $scope;

  describe('with httpBackend', function() {
    beforeEach(inject(function($controller, $rootScope, $httpBackend) {
      $scope = $rootScope.$new();
      
      $httpBackend.when('GET', 'http://gdata.youtube.com/feeds/api/playlists/B2A4E1367126848D?v=2&alt=json')
        .respond(
          {}
        );

      MainCtrl = $controller('MainCtrl', { $scope: $scope });
      $httpBackend.flush();
    }));
    
    it('expecting movies', function() {
      expect(angular.isArray($scope.movies)).toBeTruthy()
    });
  });
});