'use strict';

angular.module('movieBase')
  .controller('MainController', function ($scope, $log, youtubeService,infoService, storageService) {
    var vm = this;

    vm.youtube = {
        channels : [],
        playlists : storageService.youtube.getPlaylists(),
        videos : storageService.youtube.getVideos(),
    }

    storageService.youtube.getChannels().filter(function(item){
        youtubeService.getChannelInfo(item,function(results){
            vm.youtube.channels.push(results[0]);
        });
        return false; 
    });
  });
