'use strict';

angular.module('movieBase')
       .service('resourcesService', resourcesService);

function resourcesService($resource, youtubeConsts) 
{
    var resourcesService = this;
    /* * * * * * * * * * * * * * * * *
     * YOUTUBE Resources Definition  *
     * * * * * * * * * * * * * * * * */
    var youtubeResource = {
        // playlistItems?part=snippet&maxResults=50&playlistId={PLAYLIST_IDs}&key={YOUR_API_KEY}
        playlistItems   : $resource(youtubeConsts.apiEndpoints.playlistItems, 
                                    {
                                        part: 'snippet', 
                                        maxResults: '50', 
                                        playlistId: '@playlistId', 
                                        key: youtubeConsts.apiKey
                                    }, 
                                    {
                                        // Let's make the `query()` method cancellable
                                        query: {
                                            method: 'get', 
                                            isArray: true, 
                                            cancellable: true
                                        }
                                    }),
        // videos?part=id%2Csnippet&id={VIDEO_IDs}&key={YOUR_API_KEY}
        videos          : $resource(youtubeConsts.apiEndpoints.videos, 
                                    {
                                        part: 'id,snippet', 
                                        maxResults: '50', 
                                        id: '@id', 
                                        key: youtubeConsts.apiKey
                                    }, 
                                    {
                                        query: {
                                            method: 'get', 
                                            isArray: true, 
                                            cancellable: true}
                                    }), 
        // channels?part=id&forUsername={USER}&maxResults=50&key={YOUR_API_KEY}
        userChannels    : $resource(youtubeConsts.apiEndpoints.channels, 
                                    {
                                        part: 'id,snippet', 
                                        forUsername: '@forUsername', 
                                        maxResults: '50', 
                                        key: youtubeConsts.apiKey
                                    }, 
                                    {
                                        query: {
                                            method: 'get', 
                                            isArray: true, 
                                            cancellable: true
                                        }
                                    }),
        // channels?part=snippet&key={YOUR_API_KEY}&id={CHANNEL_ID}
        channelInfo    : $resource(youtubeConsts.apiEndpoints.channels, 
                                    {
                                        part: 'id,snippet', 
                                        id: '@id', 
                                        key: youtubeConsts.apiKey
                                    }, 
                                    {
                                        query: {
                                            method: 'get', 
                                            isArray: true, 
                                            cancellable: true
                                        }
                                    }),
        
        // playlists?part=snippet&channelId={CHANNEL_ID}&key={YOUR_API_KEY}
        playlists       : $resource(youtubeConsts.apiEndpoints.playlists, 
                                    {
                                        part: 'snippet', 
                                        channelId: '@channelId', 
                                        maxResults: '50', 
                                        key: youtubeConsts.apiKey
                                    }, 
                                    {
                                        query: {
                                            method: 'get', 
                                            isArray: true, 
                                            cancellable: true
                                        }
                                    }),
        // subscriptions?part=snippet&forChannelId={CHANNEL_IDs}&key={YOUR_API_KEY}
        // subscriptions?part=snippet&channelId={CHANNEL_ID}&key={YOUR_API_KEY}
        subscriptions       : $resource(youtubeConsts.apiEndpoints.subscriptions, 
                                    {
                                        part: 'snippet', 
                                        channelId: '@channelId', 
                                        //maxResults: '50', 
                                        key: youtubeConsts.apiKey
                                    }, 
                                    {
                                        query: {
                                            method: 'get', 
                                            isArray: true, 
                                            cancellable: true
                                        }
                                    }),
    };

    /* * * * * * * * * * * * * * * * *
     * Query Youtube Resources       *
     * * * * * * * * * * * * * * * * */
    resourcesService.youtube = {
        user : function(user) {
            return {
                getUserChannels : function(user) {
                    return youtubeResource.userChannels.get({forUsername:user});
                }
            }
        },
        channel : function(id) {
            return {
                getInfo         : function() {
                    return youtubeResource.channelInfo.get({id:id});
                },
                getPlaylists    : function() {
                    return youtubeResource.playlists.get({channelId:id});
                },
                getSubscriptions: function() {
                    return youtubeResource.subscriptions.get({channelId:id});
                }
            }

        },
        playlist : function(playlistId) {
            return {
                getItems : function() {
                    return youtubeResource.playlistItems.get({playlistId:playlistId});
                }
            }
        }
    }

}
