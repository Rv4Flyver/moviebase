'use strict';

angular.module('movieBase')
  .service('storageService', function ($q,$log,youtubeService) {
    var vm = this;

    vm.youtube = {
        channels    : ["UCiTaPPadx3L3Q85WtKWaQcg","UCO1cgjhGzsSYb1rsB4bFe4Q","UCcOkA2Xmk1valTOWSyKyp4g","UCEm0dzPCSo97G3d6cWhbY9g"],
    	playlists   : ["PLyiDf91_bTEgnBN0jAvzNbqzrlMGID5WA","FLiTaPPadx3L3Q85WtKWaQcg"],
    	videos      : ["P1lyhkI6ba8"],

        getChannels : function() {
            return vm.youtube.channels;
        },
        getPlaylists : function() {
            return vm.youtube.playlists;
        },
        getVideos : function() {
            return vm.youtube.videos;
        }
    };
});
