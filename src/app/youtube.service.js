'use strict';

angular.module('movieBase')
  .service('youtubeService', function ($q,$log,resourcesService) {
    var vm = this;

    vm.user = {

    };

    vm.channel = {

    };

    vm.getPlaylist = function(id) {
        return {
            getVideos : function(callback) {
                return deferFunc(
                    resourcesService.youtube.playlist(id).getItems,
                    function(item) {
                        return {
                            title:        item.snippet.title,
                            description:  item.snippet.description,
                            thumbnails : {
                                default:    item.snippet.thumbnails.default,
                                p320:       item.snippet.thumbnails.medium,
                                p480:       item.snippet.thumbnails.high,
                                p640:       item.snippet.thumbnails.standard,
                                p720:       item.snippet.thumbnails.maxres
                            },
                            videoId: item.snippet.resourceId.videoId
                        };
                    },
                    callback,
                    'resourcesService.youtube.playlist('+id+').getItems()'
                );
            }
        };
    }

    vm.getPlaylistItems = function(id, callback) { // refactoring
        return deferFunc(
            resourcesService.youtube.playlist(id).getItems,
            function(item) {
                return {
                    title:        item.snippet.title,
                    description:  item.snippet.description,
                    thumbnails : {
                        default:    item.snippet.thumbnails.default,
                        p320:       item.snippet.thumbnails.medium,
                        p480:       item.snippet.thumbnails.high,
                        p640:       item.snippet.thumbnails.standard,
                        p720:       item.snippet.thumbnails.maxres
                    },
                    videoId: item.snippet.resourceId.videoId
                };
            },
            callback,
            'resourcesService.youtube.playlist('+id+').getItems()'
        );
    }

    vm.getChannelInfo = function(id, callback) {
        return deferFunc(
            resourcesService.youtube.channel(id).getInfo,
            function(item){
                return {
                    id          : item.id,
                    title       : item.snippet.title,
                    publishedAt : item.snippet.publishedAt,
                    description :  item.snippet.description,
                    thumbnails  : {
                        default:    item.snippet.thumbnails.default || null,
                        p320:       item.snippet.thumbnails.medium  || null,
                        p480:       item.snippet.thumbnails.high || null,
                        p640:       item.snippet.thumbnails.standard || null,
                        p720:       item.snippet.thumbnails.maxres || null
                    },
                };
            },
            callback,
            'resourcesService.youtube.channel(('+id+').getInfo()'
        );
    }

    vm.getChannelPlaylists = function(id, callback) {
        return deferFunc(
            resourcesService.youtube.channel(id).getPlaylists,
            function(item){
                return {
                    id          : item.id,
                    title       : item.snippet.title,
                    publishedAt : item.snippet.publishedAt,
                    description :  item.snippet.description,
                    thumbnails  : {
                        default:    item.snippet.thumbnails.default,
                        p320:       item.snippet.thumbnails.medium,
                        p480:       item.snippet.thumbnails.high,
                        p640:       item.snippet.thumbnails.standard,
                        p720:       item.snippet.thumbnails.maxres
                    },
                };
            },
            callback,
            'resourcesService.youtube.channel(('+id+').getPlaylists()'
        );
    }

    vm.getChannelSubscriptionChannels = function(id, callback) {
        return deferFunc(
            resourcesService.youtube.channel(id).getSubscriptions,
            function(item){
                return {
                    id          : item.snippet.resourceId.channelId,
                    title       : item.snippet.title,
                    publishedAt : item.snippet.publishedAt,
                    description :  item.snippet.description,
                    thumbnails  : {
                        default:    item.snippet.thumbnails.default,
                        p320:       item.snippet.thumbnails.medium,
                        p480:       item.snippet.thumbnails.high,
                        p640:       item.snippet.thumbnails.standard,
                        p720:       item.snippet.thumbnails.maxres
                    },
                };
            },
            callback,
            'resourcesService.youtube.channel(('+id+').getSubscriptions()'
        );
    }

    /************************************************************
     * Defer Function
     *    make deferred request to specified resource
     *    maps responce items according specified responceMapFunc
     *    invokes callback function
     *    print short debug messages
     ************************************************************/
    function deferFunc(resource,responceMapFunc,callback,debugInfo) {
        var deferredObj = $q.defer();
        resource().$promise.then( 
           function(response){ // success
                $log.log(' > '+ debugInfo +': ' + response);

                var results = response.items.map(responceMapFunc);
                
                $log.log("   requested data received.");
                deferredObj.resolve(results);

                if(callback) 
                {
                    callback(results);
                }
           },
           function(response) { // fail
                $log.log(' x ' + debugInfo + ': ' + response);
                deferredObj.reject(response);
           }
        );
        return deferredObj.promise;
    }
});
