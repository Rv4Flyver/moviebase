'use strict';

angular.module('movieBase')
  .controller('BaseController', function ($scope, $log, $state,infoService) {
    $state.go("base.home");

    var vm = this;

    vm.moviesCount = infoService.info.stats.movies.count;

    infoService.subscribeOnUserDataChanges($scope, function somethingChanged() {
        vm.moviesCount = infoService.info.stats.movies.count;
        vm.playlistsCount = infoService.info.stats.playlists.count;
    });

  });
