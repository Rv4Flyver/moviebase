'use strict';

angular.module('movieBase')
  .controller('ChannelWatchController', function ($log,$scope,$stateParams,youtubeService,infoService,storageService) {
    var vm = this;
    var playlists = [];
    vm.id = $stateParams.id;

    // pagination
    vm.currentPage = 1;
    vm.totalItems = 0;
    vm.maxSize = 8;
    vm.bigTotalItems = 175;
    vm.bigCurrentPage = 1;
    
    vm.channels = "";
    vm.playlists = [
      // {
      //   title : '',
      //   description: '',
      //   thumbnails: {
      //     p90 : '',
      //     p180: '',
      //     p360: '',
      //     p480: '',
      //     p720: ''
      //   },
      //   id: ''
      // }
    ];
    //var fetchedResult = {};

    youtubeService.getChannelPlaylists(vm.id, function (result) {
        $log.log('> youtubeService.getChannelPlaylists:');
        $log.info(result);

        playlists = result;
        vm.totalItems = result.length;
        vm.pageChanged();
    });

    vm.playlists = vm.getPlaylists = function() {
        youtubeService.getPlaylistItems(vm.playlists, function (result) {
            $log.log('> youtubeService.getPlaylist:');
            $log.info(result);

            playlists = result;
            vm.totalItems = result.length;
            vm.pageChanged();
        });

        // // Get playlists in loop
        // var playlistsArr = vm.playlists.split(',');
        // for(var p in playlistsArr) {
        //     youtubeService.getPlaylist(playlistsArr[p], function (result) {
        //         $log.log('> youtubeService.getPlaylist:');
        //         $log.info(result);

        //         channels = channels.concat(result);
        //         vm.totalItems =  channels.length; 
        //         vm.pageChanged();
        //     });
        // }
    }

    vm.pageChanged = function() {
        vm.playlists = playlists.slice(vm.maxSize*(vm.currentPage-1),vm.maxSize*(vm.currentPage-1)+vm.maxSize);
        infoService.setPlaylistsCount(playlists.length);
    };


    angular.forEach($scope.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
    });
  });
