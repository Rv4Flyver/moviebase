'use strict';

angular.module('movieBase')
  .controller('PlaylistWatchController', function ($log,$scope,$stateParams,youtubeService,infoService,storageService) {
    var vm = this;
    var movies = [];
    vm.id = $stateParams.id;
    // pagination
    vm.currentPage = 1;
    vm.totalItems = 0;
    vm.maxSize = 8;
    vm.bigTotalItems = 175;
    vm.bigCurrentPage = 1;
    
    vm.movies = [
      // {
      //   title : '',
      //   description: '',
      //   thumbnails: {
      //     p90 : '',
      //     p180: '',
      //     p360: '',
      //     p480: '',
      //     p720: ''
      //   },
      //   videoId: ''
      // }
    ];
    //var fetchedResult = {};

    youtubeService.getPlaylistItems(vm.id, function (result) {
        $log.log('> youtubeService.getPlaylist:');
        $log.info(result);

        movies = result;
        vm.totalItems = result.length;
        vm.pageChanged();
    });

    vm.pageChanged = function() {
        vm.movies = movies.slice(vm.maxSize*(vm.currentPage-1),vm.maxSize*(vm.currentPage-1)+vm.maxSize);
        infoService.setMoviesCount(movies.length);
    };


    angular.forEach($scope.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
    });
  });
